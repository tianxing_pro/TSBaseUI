import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import { FlatList, FlatListProps } from "react-native";

interface PageProps {
  page?: number;
  size?: number;
}

interface ResponseType<ItemT> {
  list: ItemT[];
  page: number;
  total: number;
}

interface Response<ItemT> {
  data?: ResponseType<ItemT>;
  code: number;
  message: string;
  needCheck?: number;
  subCode?: string;
  fieldPermission?: any;
}

interface Props<ItemT, Variables> extends Omit<FlatListProps<ItemT>, "data"> {
  query: (params: PageProps & Variables) => Promise<Response<ItemT>>;
  variables: Variables & PageProps;
  data?: ItemT[];
  convert?: (data?: ResponseType<ItemT>) => ItemT[];
}

export default function PagingList<ItemT, Variables>(
  props: Props<ItemT, Variables>
) {
  const { query, variables, data, convert } = props;
  const { page = 1 } = variables;
  const [interPage, setInterPage] = useState<number>(page);
  const totalRef = useRef<number>();
  const [items, setItems] = useState<ItemT[]>([]);
  const itemsRef = useRef<ItemT[]>([]);
  const loadRef = useRef<boolean>(false);
  const [refreshing, setRefreshing] = useState<boolean>(false);

  const request = useMemo(() => {
    loadRef.current = true;
    query({ ...variables, page: interPage })
      .then((res) => {
        const list = convert ? convert?.(res?.data) : res?.data?.list || [];
        itemsRef.current = [...itemsRef.current, ...list];
        setItems(itemsRef.current);
        loadRef.current = false;
        setRefreshing(false);
        if (!res?.data?.total) {
          throw Error("总数不能为空");
        }
        console.log("我被触发了 --  res.data.total", res.data.total);
        totalRef.current = res?.data?.total!;
      })
      .catch((error) => {
        console.log(error);
      });
  }, [convert, interPage, query, variables]);

  const refresh = useCallback(() => {
    setInterPage(1);
    setItems([]);
    itemsRef.current = [];
    setRefreshing(true);
  }, []);

  useEffect(() => request, [request]);

  const renderData = React.useMemo(() => {
    return <FlatList data={data} {...props} />;
  }, [data, props]);

  const onEndReached = useCallback(() => {
    if (itemsRef.current.length >= totalRef.current! && loadRef.current) return;
    setInterPage(interPage + 1);
  }, [interPage]);

  const renderNetWorkData = React.useMemo(() => {
    return (
      <FlatList
        data={items}
        onEndReachedThreshold={0.2}
        onEndReached={onEndReached}
        onRefresh={refresh}
        refreshing={refreshing}
        {...props}
      />
    );
  }, [items, onEndReached, props, refresh, refreshing]);

  return <>{data ? renderData : renderNetWorkData}</>;
}
